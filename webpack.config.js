const webpack = require('webpack');

var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');

const devBuild = process.env.NODE_ENV !== 'production';
const nodeEnv = devBuild ? 'development' : 'production';
const isDevServer = process.argv[1].indexOf('webpack-dev-server') !== -1;

const sassLoaders = [
    'css-loader?importLoaders=1',
    'postcss-loader',
    'sass-loader'
];

var config = {
    entry: [
        './App/startup/app'
    ],
    output: {
        path: __dirname+'/public/',
        publicPath: '/',
        filename: 'app.js'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    plugins: [
        new ExtractTextPlugin('stylesheets/styles.css'),
        new webpack.ProvidePlugin({
            _: 'lodash',
            $: 'jquery',
            'jQuery'              : 'jquery',
            'window.jQuery'       : 'jquery'
        }),
        new CopyWebpackPlugin([
            { from: './App/res/img', to: './img' }
        ]),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(nodeEnv)
            }
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    module: {
        loaders: [
            {test: require.resolve("jquery"), loader: "expose?$!expose?jQuery" },
            {test: /\.jsx?$/, loader: 'babel-loader' , exclude: /node_modules/ },
            {test: /\.scss$/i, loader: isDevServer ? ('style-loader!'+sassLoaders.join('!')) : ExtractTextPlugin.extract({fallbackLoader: 'style-loader',loader: sassLoaders.join('!') })},
            {test: /\.css/i, loader: isDevServer ? ('style-loader!'+sassLoaders.join('!')) : ExtractTextPlugin.extract({fallbackLoader: 'style-loader',loader: sassLoaders.join('!') })},
            {test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/font-woff'},
            {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=application/octet-stream'},
            {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file'},
            {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url?limit=10000&mimetype=image/svg+xml'},
            {test: /\.jpe?g$/, loader: 'file'},
            {test: /\.png?$/, loader: 'file'}
        ]
    },
    target: "web",
    node: {
        console: true,
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    },
    devServer: {
        hot: true,
        contentBase: './public',
        inline: true
    }
};

if (devBuild) {
    console.log('Webpack dev build');
    config.devtool = '#eval-source-map';
} else {
    config.plugins.push(
        new webpack.optimize.DedupePlugin()
    );
    console.log('Webpack production build');
}

module.exports = config;
