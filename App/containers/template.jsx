import React from 'react';
import Actions from '../actions/actions';
import { connect } from 'react-redux';
import Template from '../components/Template/template';

const mapStateToProps = (state) => {
    return {
        smth: state.other.hello
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        change: () => {
            dispatch(Actions.change());
        }
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Template);
