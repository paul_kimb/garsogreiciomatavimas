import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import configureStore from '../store/store';
import configureRoutes from '../routes';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore, routerReducer } from 'react-router-redux';

var mainNode = () => {
    const store = configureStore(browserHistory);

    const reactComponent = (
        <Provider store={store}>
            <Router history={browserHistory}>
                {configureRoutes(store)}
            </Router>
        </Provider>
    );
    return reactComponent;
};

ReactDOM.render(mainNode(),
    document.getElementById('app'));
