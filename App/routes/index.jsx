import { IndexRoute, Route } from 'react-router';
import React from 'react';

import Template from '../containers/template';

export default function configureRoutes(store) {
    return (
        <div>
            <Route path="boilerplateGen/" component={Template}></Route>
        </div>
    )
};
