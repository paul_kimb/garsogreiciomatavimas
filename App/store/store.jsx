import { compose, createStore, applyMiddleware, combineReducers } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import createLogger from 'redux-logger';
import _ from 'lodash';

import reducer from '../reducers';
import { initialStates } from '../reducers';

const loggerMiddleware = createLogger({
    level: 'info',
    collapsed: true,
});

export default function configureStore(props, browserHistory) {

    const { homeState } = initialStates;

    // Redux expects to initialize the store using an Object
    const initialState = {
        homeState
    };

    const reduxRouterMiddleWare = routerMiddleware(browserHistory)

    const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;
    const store = createStore(
        reducer,
        composeEnhancers(applyMiddleware(reduxRouterMiddleWare, loggerMiddleware))
    );

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            System.import('../reducers').then((reducerModule) => {
                console.log(reducerModule);
                const createReducers = reducerModule.default;
                store.replaceReducer(createReducers);
            });

        });
    }
    return store;
}
