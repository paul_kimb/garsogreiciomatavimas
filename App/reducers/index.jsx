import { combineReducers } from 'redux';
import dashReducer from './dashReducer';
import otherReducer from './otherReducer';
import { initialState as dashState } from './dashReducer';
import { initialState as otherState } from './otherReducer';

//Combine all reducers
export default combineReducers({
    dash: dashReducer,
    other: otherReducer
});

//initialStates of all reducers
export const initialStates = {
    dashState,
    otherState
};
