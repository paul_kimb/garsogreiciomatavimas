import Constants from '../constants/constants';

export const initialState = {
    hello: 'helloo'
};

export default function otherReducer(state = initialState, action){
    switch (action.type){
        case (Constants.CHANGE):
            return {...state, hello: 'hello2'};
        default:
            return state;
    }
}
