import Constants from '../constants/constants';

export const initialState = {
    hello: 'helloo'
};

export default function dashReducer(state = initialState, action){
    switch (action.type){
        case (Constants.CHANGE):
            return {...state, hello: 'hello1'};
        default:
            return state;
    }
}
