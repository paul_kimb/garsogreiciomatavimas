module.exports = (ctx) => {
    return {
        plugins: [
            require('postcss-smart-import')(),
            require('autoprefixer')()
        ]
    }
}
